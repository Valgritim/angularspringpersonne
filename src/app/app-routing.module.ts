import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonneFormComponent } from './personne-form/personne-form.component';
import { PersonneListComponent } from './personne-list/personne-list.component';


const routes: Routes = [

  { path:'addPersonne', component: PersonneFormComponent },
  { path: 'personnes', component: PersonneListComponent},
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
