import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { PersonneListComponent } from './personne-list/personne-list.component';
import { PersonneFormComponent } from './personne-form/personne-form.component';
import { PersonneServiceService } from './personne-service.service';

@NgModule({
  declarations: [
    AppComponent,
    PersonneListComponent,
    PersonneFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [PersonneServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
