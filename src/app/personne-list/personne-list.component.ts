import { Component, OnInit } from '@angular/core';
import { Personne} from '../personne';
import { PersonneServiceService } from '../personne-service.service';


@Component({
  selector: 'app-personne-list',
  templateUrl: './personne-list.component.html',
  styleUrls: ['./personne-list.component.css']
})
export class PersonneListComponent implements OnInit {

  personnes: Personne[];
  
  constructor(private personneService: PersonneServiceService ) { }

  ngOnInit(): void {
    this.personneService.findAll().subscribe(data=> {
      this.personnes = data;
    });

  }

}
