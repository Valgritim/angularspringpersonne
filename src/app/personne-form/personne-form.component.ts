import { Component} from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { PersonneServiceService } from '../personne-service.service';
import { Personne } from '../personne';

@Component({
  selector: 'app-personne-form',
  templateUrl: './personne-form.component.html',
  styleUrls: ['./personne-form.component.css']
})
export class PersonneFormComponent {

  personne: Personne;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private personneService: PersonneServiceService) { 
      this.personne = new Personne();
  }

  onSubmit(){
    this.personneService.save(this.personne).subscribe(result => this.gotoPersonneList());
  }
  gotoPersonneList(){
    this.router.navigate(['/personnes']);
  }
}
