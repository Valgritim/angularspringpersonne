import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Personne } from '../app/personne';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonneServiceService {

  private personnesUrl: string;

  constructor(private http: HttpClient) {
      this.personnesUrl='http://localhost:8080/api/v1/personnes';
   }

   public findAll():Observable<Personne[]>{
     return this.http.get<Personne[]>(this.personnesUrl);
   }

   public save(personne:Personne){
     return this.http.post<Personne>(this.personnesUrl,personne);
   }
}
